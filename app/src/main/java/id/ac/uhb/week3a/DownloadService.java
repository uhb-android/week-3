package id.ac.uhb.week3a;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class DownloadService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        Uri data = intent.getData();
        Log.d("Service",String.valueOf(data));

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Service","Start Service");
    }
}
