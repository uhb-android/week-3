package id.ac.uhb.week3a;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import id.ac.uhb.week3a.databinding.ActivityMainBinding;
import id.ac.uhb.week3a.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {
    private static final String TAG = "Second Activity";
    private ActivitySecondBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"On Create Second Activity");
        binding = ActivitySecondBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setTitle("This is Page Second");
        binding.btnGotoMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoMainActivity();
            }
        });
    }

    private void gotoMainActivity() {
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Main","On Start Second Activity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"On Stop Second Activity");
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG,"On Destroy Second Activity");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"On Pause Second Activity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"On Resume Second Activity");
    }
}