package id.ac.uhb.week3a;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import id.ac.uhb.week3a.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "Main Activity";
    private ActivityMainBinding binding;
    Button btn1;
    Button btn2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"On Create Main Activity");
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        setTitle("Belajar Pemrograman IV - Mobile");
        binding.btnGotoSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoSecondActivity();
            }
        });
        binding.btnGotoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unduhBerkas();
            }
        });
//        setContentView(R.layout.activity_main);
//        btn1 = findViewById(R.id.btn_goto_second);
//        btn2 = findViewById(R.id.btn_goto_home);
//        btn1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gotoSecondActivity();
//            }
//        });
//        btn2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this,"Maunya ke Home Saja",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
    }
    private void gotoSecondActivity(){
        Intent intent = new Intent(this,SecondActivity.class);
        startActivity(intent);
    }
    private void unduhBerkas(){
        String fileUrl = "This is Url File to download";
        Intent downloadIntent = new Intent(this, DownloadService.class);
        downloadIntent.setData(Uri.parse(fileUrl));
        startService(downloadIntent);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG,"On Start Main Activity");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"On Stop Main Activity");
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG,"On Destroy Main Activity");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"On Paused Main Activity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"On Resume Main Activity");
    }
}