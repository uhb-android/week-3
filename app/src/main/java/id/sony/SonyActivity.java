package id.sony;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import id.ac.uhb.week3a.R;

public class SonyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sony);
    }
}